/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test.httpserver;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;

/**
 *
 * @author ixm
 */
public class HttpServerTest {
    protected final Logger logger = LogManager.getLogger(HttpServerTest.class);

    protected final Pattern uuidPattern = Pattern.compile("([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})");

    @Test
    public void serve_whenWrongURL_thenReturnBadRequest() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer spy_server = spy(server);

        String uri = "http://localhost/^";

        Response response = spy_server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of("name", "Maximilien"), Map.of());

        try {
            verify(spy_server, Mockito.times(1)).getRequestType(uri);
        } catch(Exception e) {}

        verify(spy_server, Mockito.never()).discovery(any());
        verify(spy_server, Mockito.never()).uuidGenerator();

        Assertions.assertEquals(Response.Status.BAD_REQUEST, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("Could not parse URI: Illegal character in path at index 17: http://localhost/^", retStr);
    }

    @Test
    public void serve_whenDiscovery_thenDiscoveryIsCalled() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer spy_server = spy(server);

        String uri = "http://localhost/discovery";

        when(spy_server.discovery(any(Map.class))).thenReturn(NanoHTTPD.newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, "expectedReturn"));

        Response response = spy_server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of("name", "Maximilien"), Map.of());

        Assertions.assertDoesNotThrow(() -> verify(spy_server, Mockito.times(1)).getRequestType(uri));
        verify(spy_server, Mockito.times(1)).discovery(Map.of("name", "Maximilien"));
        verify(spy_server, Mockito.never()).uuidGenerator();

        Assertions.assertEquals(Response.Status.OK, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("expectedReturn", retStr);
    }

    @Test
    public void serve_whenUUID_thenUUIDGeneratorIsCalled() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer spy_server = spy(server);

        String uri = "http://localhost/uuid";

        when(spy_server.uuidGenerator()).thenReturn(NanoHTTPD.newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, "expectedReturn"));

        Response response = spy_server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of(), Map.of());

        Assertions.assertDoesNotThrow(() -> verify(spy_server, Mockito.times(1)).getRequestType(uri));
        verify(spy_server, Mockito.never()).discovery(any());
        verify(spy_server, Mockito.times(1)).uuidGenerator();

        Assertions.assertEquals(Response.Status.OK, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("expectedReturn", retStr);
    }

    @Test
    public void serve_whenOther_thenReturnNotFound() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer spy_server = spy(server);

        String uri = "http://localhost/other";

        when(spy_server.uuidGenerator()).thenReturn(NanoHTTPD.newFixedLengthResponse(Response.Status.OK, NanoHTTPD.MIME_PLAINTEXT, "expectedReturn"));

        Response response = spy_server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of(), Map.of());

        Assertions.assertDoesNotThrow(() -> verify(spy_server, Mockito.times(1)).getRequestType(uri));
        verify(spy_server, Mockito.never()).discovery(any());
        verify(spy_server, Mockito.never()).uuidGenerator();

        Assertions.assertEquals(Response.Status.NOT_FOUND, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("Unknown request type", retStr);
    }

    @Test
    public void getRequestType_whenDiscovery_thenReturnDiscoveryRequestType() {
        String uri = "http://localhost/discovery";

        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer.RequestType requestType = Assertions.assertDoesNotThrow(() -> server.getRequestType(uri));

        Assertions.assertEquals(HttpServer.RequestType.DISCOVERY, requestType);
    }

    @Test
    public void getRequestType_whenUUID_thenReturnUUIDGeneratorRequestType() {
        String uri = "http://localhost/uuid";

        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer.RequestType requestType = Assertions.assertDoesNotThrow(() -> server.getRequestType(uri));

        Assertions.assertEquals(HttpServer.RequestType.UUID_GENERATOR, requestType);
    }

    @Test
    public void getRequestType_whenOther_thenReturnUnknownRequestType() {
        String uri = "http://localhost/other";

        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer.RequestType requestType = Assertions.assertDoesNotThrow(() -> server.getRequestType(uri));

        Assertions.assertEquals(HttpServer.RequestType.UNKNOWN, requestType);
    }

    @Test
    public void getRequestType_whenEmptyPath_thenReturnUnknownRequestType() {
        String uri = "http://localhost";

        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        HttpServer.RequestType requestType = Assertions.assertDoesNotThrow(() -> server.getRequestType(uri));

        Assertions.assertEquals(HttpServer.RequestType.UNKNOWN, requestType);
    }

    @Test
    public void getRequestType_whenWrongURL_thenThrowException() {
        String uri = "http://other^";

        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        Exception e = Assertions.assertThrows(Exception.class, () -> server.getRequestType(uri));

        logger.info("Error: "+ e.getMessage());

        Assertions.assertTrue(e.getMessage().startsWith("Could not parse URI: "));
    }

    @Test
    public void discovery_whenParamsNull_thenReturnBadRequest() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        Response response = server.discovery(Map.of());

        Assertions.assertEquals(Response.Status.BAD_REQUEST, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("No \"name\" entry in params", retStr);
    }

    @Test
    public void discovery_whenNameAbsent_thenReturnBadRequest() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        Response response = server.discovery(Map.of());

        Assertions.assertEquals(Response.Status.BAD_REQUEST, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("No \"name\" entry in params", retStr);
    }

    @Test
    public void discovery_whenNamePresent_thenReturnHi() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        Response response = server.discovery(Map.of("name", "Maximilien"));

        Assertions.assertEquals(Response.Status.OK, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        Assertions.assertEquals("Hi Maximilien, I'm KISANO server", retStr);
    }

    @Test
    public void uuidGenerator() {
        HttpServer server = Assertions.assertDoesNotThrow(() -> new HttpServer(2375));

        Response response = server.uuidGenerator();

        Assertions.assertEquals(Response.Status.OK, response.getStatus());

        Assertions.assertEquals("text/plain", response.getMimeType());

        String retStr = Assertions.assertDoesNotThrow(() -> IOUtils.toString(response.getData(), Charset.defaultCharset()));

        logger.info(retStr);

        Matcher m = uuidPattern.matcher(retStr);

        Assertions.assertTrue(m.find());
    }
}
