/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test;

import com.kisanogroup.test.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.kisanogroup.test.thread.UUIDGeneratorThread;
import fi.iki.elonen.NanoHTTPD;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author ixm
 */
public class Main {
    public final Logger logger = LogManager.getLogger(Main.class);

    private static final List<Integer> portRange = List.of(
            2375, 2376, 2377, 2378, 2379, 2380
    );

    public static void main(String[] args) {
        HttpServer.startServer();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your first name please [default:Mulham]");
        String name = scanner.nextLine();
        name = (name.isEmpty()) ? "Mulham" : name;

        System.out.println("Enter your file path [default:/Users/mulham/testfile.txt]");
        String filePath = scanner.nextLine();
        filePath = (filePath.isEmpty()) ? "/Users/mulham/testfile.txt" : filePath;

        new Main(name, filePath);
    }

    public static int findServerPort(List<Integer> portRange, String name) {
        int i = 0;
        int currentPort = 0;
        for (int port : portRange) {
            try {
                URL url = new URL("http", "localhost", port, "/discovery?name=" + name);
                HttpURLConnection server = (HttpURLConnection) url.openConnection();
                if (server.getResponseCode() == 200) {
                    BufferedReader r = new BufferedReader(
                            new InputStreamReader(
                                    server.getInputStream(),
                                    StandardCharsets.UTF_8
                            )
                    );

                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        sb.append(line);
                    }
                    if (sb.toString().equals("Hi " + name + ", I'm KISANO server")) {
                        currentPort = port;
                        break;
                    }
                }
            } catch (IOException e) {
                if(i == (portRange.size() - 1)){
                    System.err.println("Unable to found server port !");
                    System.exit(0);
                }
            }
            i++;
        }

        return currentPort;
    }

    public Main(String name, String filePath) {
        HttpServer server = HttpServer.getInstance();
        //int currentPort = HttpServer.getCurrentPort(portRange, name); //First try - Method which detects that it is the port used by KISANO server using portRange from main
        //int currentPort = HttpServer.getPortFromInstance(); //Second try - Method getListeningPort implemented method in NanoHTTPD using current instance to find the port
        int currentPort = findServerPort(portRange, name);

        System.out.println("The current KISANO server port is : " + currentPort + " for " + name);
        String uri = "http://localhost:" + currentPort + "/discovery";
        NanoHTTPD.Response response = server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of("name", name), Map.of());
        try {
            String welcomeMessage = IOUtils.toString(response.getData(), Charset.defaultCharset());
            System.out.println(welcomeMessage);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        UUIDGeneratorThread uuidGeneratorThread = new UUIDGeneratorThread(name);
        uuidGeneratorThread.setSleepPeriod(10000);
        uuidGeneratorThread.setFilePath(filePath);
        uuidGeneratorThread.start();
    }
}
