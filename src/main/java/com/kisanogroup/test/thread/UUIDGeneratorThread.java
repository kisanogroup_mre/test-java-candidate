package com.kisanogroup.test.thread;

import com.kisanogroup.test.httpserver.HttpServer;
import fi.iki.elonen.NanoHTTPD;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;

public class UUIDGeneratorThread extends KisanoThread{
    private final Logger threadLogger = LogManager.getLogger(UUIDGeneratorThread.class);

    private String filePath = null;

    public UUIDGeneratorThread(String threadName){
        super(threadName);
    }

    @Override
    protected void doWork() {
        setLastExecutedOn(DateTime.now());
        String newUuid = null;
        int currentPort = HttpServer.getPortFromInstance();
        String uri = "http://localhost:" + currentPort + "/uuid";
        HttpServer server = HttpServer.getInstance();
        NanoHTTPD.Response getUuid = server.serve(uri, NanoHTTPD.Method.GET, Map.of(), Map.of(), Map.of());
        try{
            newUuid = IOUtils.toString(getUuid.getData(), Charset.defaultCharset());
        }
        catch (Throwable e){
            setExiting(true);
            threadLogger.error("Exception in doWork(): "+ e.getMessage(), e, e.getStackTrace());
            if(e.getCause() != null)
                threadLogger.error(e.getCause().getMessage(), e.getCause());
        }
        setBusy(true);
        createFile();

        String myString = LocalDate.now() + " " + LocalTime.now() + ":" + newUuid + "\r\n";
        writeInFile(myString); // Append to file
        System.out.println(myString); //Print in terminal
    }

    //This method will create the file if doesn't exists
    public void createFile(){
        String path = this.filePath;
        try {
            File file = new File(path);
            if (!file.exists()) {
                try{
                    file.createNewFile();
                    threadLogger.info("File : " + path + " not found but successfully created :)");
                }
                catch (Throwable e){
                    setExiting(true);
                    threadLogger.error("Exception in createFile(): "+ e.getMessage(), e, e.getStackTrace());
                    if(e.getCause() != null)
                        threadLogger.error(e.getCause().getMessage(), e.getCause());
                }
            }
        } catch (Throwable e) {
            setExiting(true);
            threadLogger.error("Exception in createFile(): "+ e.getMessage(), e, e.getStackTrace());
            if(e.getCause() != null)
                threadLogger.error(e.getCause().getMessage(), e.getCause());
        }
    }

    //This method will write and append new line in the current working file
    public void writeInFile(String myText){
        try {
            Files.write(Paths.get(this.filePath), myText.getBytes(), StandardOpenOption.APPEND);
        }catch (Throwable e) {
            setExiting(true);
            threadLogger.error("Exception in createFile(): "+ e.getMessage(), e, e.getStackTrace());
            if(e.getCause() != null)
                threadLogger.error(e.getCause().getMessage(), e.getCause());
        }
    }

    //filePath setter
    public void setFilePath(String filePath){
        this.filePath = filePath;
    }

    //filePath getter
    public String getFilePath(){
        return this.filePath;
    }

    //run method of thread class
    public void run(){
        super.run();
    }
}
