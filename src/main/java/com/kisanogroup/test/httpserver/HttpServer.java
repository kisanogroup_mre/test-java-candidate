/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kisanogroup.test.httpserver;

import fi.iki.elonen.NanoHTTPD;

import java.io.IOException;
import java.net.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ixm
 */
public class HttpServer extends NanoHTTPD {
    private final Logger logger = LogManager.getLogger(HttpServer.class);

    private static final Integer[] portRange = {
        2375, 2376, 2377, 2378, 2379, 2380
    };

    public enum RequestType {
        DISCOVERY, UUID_GENERATOR, UNKNOWN
    }

    private static HttpServer server = null;

    public static HttpServer getInstance() {
        return server;
    }

    protected HttpServer(int port) throws IOException {
        super(port);
    }
    @Override
    public Response serve(String uri, Method method, Map<String, String> header, Map<String, String> params, Map<String, String> files) {
        RequestType requestType;
        try {
            requestType = getRequestType(uri);
        } catch (Exception ex) {
            return setHeaders(newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_PLAINTEXT, ex.getMessage()));
        }

        switch(requestType) {
            case DISCOVERY: {
                return setHeaders(discovery(params));
            }

            case UUID_GENERATOR: {
                return setHeaders(uuidGenerator());
            }

            default: {
                return setHeaders(newFixedLengthResponse(Response.Status.NOT_FOUND, MIME_PLAINTEXT, "Unknown request type"));
            }
        }
    }

    protected Response discovery(Map<String, String> params) {
        if(params == null || !params.containsKey("name")) {
            logger.error("No \"name\" entry in params");
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, MIME_PLAINTEXT, "No \"name\" entry in params");
        }

        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, "Hi "+ params.get("name") +", I'm KISANO server");
    }

    protected Response uuidGenerator() {
        return newFixedLengthResponse(Response.Status.OK, MIME_PLAINTEXT, UUID.randomUUID().toString());
    }

    protected RequestType getRequestType(String uri) throws Exception {
        URI uriObj;
        try {
            uriObj = new URI(uri);
        } catch (NullPointerException|URISyntaxException ex) {
            logger.error("Could not parse URI: "+ ex.getMessage(), ex);
            throw new Exception("Could not parse URI: "+ ex.getMessage(), ex);
        }

        String path = uriObj.getPath();

        switch(StringUtils.defaultString(path)) {
            case "/discovery":
                return RequestType.DISCOVERY;

            case "/uuid":
                return RequestType.UUID_GENERATOR;

            default:
                return RequestType.UNKNOWN;
        }
    }

    protected Response setHeaders(Response resp) {
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Max-Age", "3628800");
        resp.addHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        resp.addHeader("Access-Control-Allow-Headers", "X-Requested-With");
        resp.addHeader("Access-Control-Allow-Headers", "Authorization");
        return resp;
    }

    public static boolean startServer() {
        int i = 0;
        List<Integer> ports = new ArrayList<>(Arrays.asList(HttpServer.portRange));

        Collections.shuffle(ports);

        while (server == null && i < ports.size()) {
            int tmpPort = ports.get(i++);

            try {
                server = new HttpServer(tmpPort);
                server.start();
                LogManager.getRootLogger().info("HTTP server started on port: "+ tmpPort);
                return true;
            } catch (IOException ex) {
                LogManager.getRootLogger().info("Cannot listen on port: "+ tmpPort + "."+ (i + 1 >= portRange.length ? " All ports have been tested" : "Trying port: "+ portRange[i+1]), ex);
                if(server != null && server.wasStarted())
                    server.stop();
                server = null;
            }
        }

        return false;
    }

    /* ------------------------------------------------------------------------------------------------------------------------
    // ------------------------------------------------- Added Methods ----------------------------------------------------- */
    //This method will bind the URL to check if the port is used or not (WARNING if the port used by other application it might return true ! string check implemented)
    private static boolean checkPort(int port){
        try (ServerSocket serverSocket = new ServerSocket()){
            serverSocket.setReuseAddress(false);
            serverSocket.bind(new InetSocketAddress(InetAddress.getByName("localhost"), port), 1);
            return false;
        } catch (IOException e) {
            return true;
        }
    }

    // This method will check a list of port from <portRange> and <name> from Main method
    public static int getCurrentPort(List<Integer> ports,String name){
        for (int port : ports){
            if(checkPort(port) /*&& checkKisanoServerString(name)*/){
                return port;
            }
        }
        return 0;
    }

    //This method will verify if the answer from GET /discovery is correct
    public static boolean checkKisanoServerString(String name){
        String welcomeMessage = null;
        Response response = server.discovery(Map.of("name", name));
        try {
            welcomeMessage = IOUtils.toString(response.getData(), Charset.defaultCharset());
        }
        catch (IOException e){
            LogManager.getRootLogger().error(e.getMessage());
        }
        assert welcomeMessage != null;
        return welcomeMessage.equals("Hi " + name + ", I'm KISANO server");
    }

    //Get current listening port from current server instance
    public static int getPortFromInstance(){
        return server.getListeningPort();
    }
}
